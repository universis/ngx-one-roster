import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationService, SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as locales from './i18n';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [],
  exports: []
})
export class NgxOneRosterModule {
  constructor(private translate: TranslateService, private configuration: ConfigurationService) {
    this.configuration.loaded.subscribe(() => {
      Object.keys(locales).forEach((locale) => {
        if (Object.prototype.hasOwnProperty.call(locales, locale)) {
          const applicationLocales = (this.configuration.settings &&
            this.configuration.settings.i18n && this.configuration.settings.i18n.locales) || [];
          if (this.configuration.settings && this.configuration.settings.i18n) {
            if (applicationLocales.indexOf(locale) > -1) {
              this.translate.setTranslation(locale, locales[locale], true);
            }
          }
        }
      });
    });
  }
  static forRoot(): ModuleWithProviders<NgxOneRosterModule> {
    return {
      ngModule: NgxOneRosterModule,
      providers: []
    }
  }
 }
