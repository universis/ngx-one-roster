export const en = {
  "OneRoster": {
    "IncomingResults": "Incoming results",
    "OutgoingResults": "Outgoing results",
    "UnsupportedAlert": "Student incoming results management is not active to local campus management system",
    "MissingProviderAlert": "The service provider for incoming results has not been set. Incoming results processing is unavailable.",
    "OriginalGradingScheme": "Grading scheme provided by the service provider",
    "OriginalGradingSchemeBase": "The minimum value -based on the given grading scheme- of a passing grade",
    "OriginalScore": "The original grade based on the given grading scheme",
    "AlternateScore": "The grade converted to the base grading scheme of the local institution",
    "LoadingMessage": "Loading incoming results...",
    "AssignGrades": "Accept and assign",
    "EmptyResults": {
      "Title": "There are no incoming results",
      "Message": "The service provider did not return any record associated with the selected student."
    },
    "ActionStatusTypes": {
        "ActiveActionStatus": "Active",
        "CancelledActionStatus": "Cancelled",
        "CompletedActionStatus": "Completed",
        "FailedActionStatus": "Failed",
        "PotentialActionStatus": "Pending",
        "null": "-"
      },
      "Status": "Status",
      "StartTime": "Started at",
      "EndTime": "Ended at",
      "CreatedBy": "Created by",
      "CreatedAt": "Created at",
      "ModifiedBy": "Modified by",
      "ModifiedAt": "Modified at",
      "AcademicYear": "Academic year",
      "AcademicPeriod": "Academic period",
      "Name": "Name",
      "Departments": "Departments",
      "Run": "Run",
      "Refresh": "Refresh",
      "OneRosterSyncAction": "Execute synchronization",
      "OneRosterSyncActions": "Synchronization actions",
    "ExecuteSyncAction": "Execute sync action",
    "ExecuteSyncActionDescription": "The operation will try to create a new synchronization action for the given academic year and period. This action will try to collect all the required data and create or update OneRoster entities.",
    "ExecuteConfirmMessageOne": "You are about to execute the synchronization of one department for the academic year {{academicYear}} and academic period {{academicPeriod}}. Are you sure you want to continue?",
    "ExecuteConfirmMessageMany": "You are about to execute the synchronization of {{departments}} department for the academic year {{academicYear}} and academic period {{academicPeriod}}. Are you sure you want to continue?",
    "ExecuteSubmitMessage": "The synchronization action has started successfully.",
    "SelectAcademicYearPeriod": "You must select an academic year and period",
    "Monitor": "Action monitor",
    "NoActiveAction": "There is no active synchronization action to monitor",
    "DepartmentList": {
        "Description": "The following departments are available for selection. Please select the departments you wish to include in the synchronization action."
      },
      "SynActionDefaultNameOne": "Exporting data of {{departments}} department for the academic year {{academicYear}} and academic period {{academicPeriod}}",
      "SynActionDefaultNameMany": "Exporting data of {{departments}} departments for the academic year {{academicYear}} and academic period {{academicPeriod}}",
      "NoDescription":"No description",
      "RunAgain": "Run again",
      "IndivisibleCourseClass": "Indivisible course class",
    "MarkClassAsIndivisible": "Mark class as indivisible",
    "RemoveIndivisibilityStatus": "Remove indivisibility status",
    "IndivisibleCourseClassDescription": "A course class that is marked as indivisible cannot be split into multiple sections. OneRoster services will export an indivisible course class as a single class even if it contains class sections. The class sections will be exported as inactive classes.",
    "OneRosterTools": "OneRoster Tools",
    "ReplaceClasses": "Replace classes",
    "ReplacedByClass": "Replaced by class",
    "DivideClass": "Divide class",
    "DivideClassDescription": "OneRoster services use class divisions as a post-processing operation for classes which are divided into multiple parts. This operation is important for classes which are being taught as multiple classes even if they don't have any class sections.",
    "NoDivisions": "This class is not divided into parts.",
    "DivideClassListDescription": "This class is divided into the following parts:",
    "Add": "Add (+)",
    "Save": "Save",
    "NoClassSectionsMessage": "A course class without sections cannot be marked as indivisible.",
    "CannotDivideClassMessage": "A course class with sections cannot be divided into parts.",
    "CannotReplaceClassMessage": "The class cannot replace other classes because it's being replaced by another class.",
    "CannotBeReplacedByMessage" : "The class cannot be replaced by other classes because it's replacing another class.",
    "NewPart": "New class part",
    "ReplaceClassesDescription": "OneRoster services use class replacements for merging class roster and mark replaced classes as inactive. This operation is important for classes which are being taught as a single class.",
    "ReplacedBClassDescription": "OneRoster services use class replacements for merging class roster and mark replaced classes as inactive. This operation is important for classes which are being taught as a single class.",
    "ShowInactiveParts": "Show inactive",
    "HideInactiveParts": "Hide inactive",
    "ActivatePart": "Activate",
    "LetSectionActive": "The class sections will be activated and exported as active classes."
  },
  "Settings": {
    "Lists": {
      "OneRosterSyncAction": {
        "Description": "OneRoster Synchronization Services",
        "LongDescription": "Tools and services for exporting student data (courses, classes, teachers, enrolments etc) based on OneRoster specification"
      }
    }
  }
}
