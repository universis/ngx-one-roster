import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActionListComponent } from './components/action-list/action-list.component';
import { ActionViewComponent } from './components/action-view/action-view.component';
import { ActionNewComponent } from './components/action-new/action-new.component';


const routes: Routes = [
  {
    path: 'actions',
    component: ActionListComponent
  },
  {
    path: 'actions/new',
    component: ActionNewComponent
  },
  {
    path: 'actions/:id',
    component: ActionViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgxOneRosterAdminRoutingModule { }
