import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { IncomingResultsComponent } from './components/incoming-results/incoming-results.component';
import { ServiceIncomingResultsComponent } from './components/incoming-results/service-incoming-results.component';
import { OutgoingResultsComponent } from './components/outgoing-results/outgoing-results.component';
import { ActionListComponent } from './components/action-list/action-list.component';
import { ActionViewComponent } from './components/action-view/action-view.component';
import { NgxOneRosterAdminRoutingModule } from './ngx-one-roster-admin-routing.module';
import { ActionNewComponent } from './components/action-new/action-new.component';
import { AdvancedFormsModule } from '@universis/forms';
import { ActionMonitorComponent } from './components/action-monitor/action-monitor.component';
import { NgArrayPipesModule } from 'ngx-pipes';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MostModule,
    TranslateModule,
    AdvancedFormsModule,
    TablesModule,
    NgxOneRosterAdminRoutingModule,
    NgArrayPipesModule
  ],
  declarations: [
    IncomingResultsComponent,
    ServiceIncomingResultsComponent,
    OutgoingResultsComponent,
    ActionListComponent,
    ActionViewComponent,
    ActionNewComponent,
    ActionMonitorComponent
  ],
  exports: [
    ActionMonitorComponent
  ]
})
export class NgxOneRosterAdminModule {
  
}
