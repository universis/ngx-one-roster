import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { from, Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';


@Component({
  selector: 'universis-one-roster-action-view',
  templateUrl: './action-view.component.html'
})
export class ActionViewComponent implements OnInit, AfterViewInit {

  model$: Observable<{
    id: string;
    name: string;
    academicYear: {
      id: string;
      name: string;
    };
    academicPeriod: {
      id: string;
      name: string;
    };
    departments: Array<{
      id: string;
      name: string;
    }>;
    createdBy: {
      id: string;
      name: string;
    };
    actionStatus: {
      alternateName: string;
    },
    dateCreated: Date;
    dateModified: Date;
  }> = this.activatedRoute.params.pipe(flatMap(({id}) => {
    return from(
      this.context.model('OneRosterSyncActions').where('id').equal(id).expand(
        'academicYear',
        'academicPeriod',
        'createdBy',
        'departments($select=id,name)'
      ).getItem()
    );
  }));

  constructor(private activatedRoute: ActivatedRoute,
    private context: AngularDataContext,
    private translate: TranslateService,
    private loading: LoadingService,
    private modal: ModalService,
    private errorService: ErrorService,
    private toast: ToastService,
    private router: Router) { }
  ngAfterViewInit(): void {
    
  }

  @Input() model: any;

  ngOnInit() {
  }

  runAgain() {
    this.model$.subscribe((current: any) => {
      const model = {
        academicYear: current.academicYear,
        academicPeriod: current.academicPeriod,
        departments: current.departments,
        name: current.name,
        actionStatus: {
          alternateName: 'PotentialActionStatus'
        }
      };
      const title = this.translate.instant('OneRoster.ExecuteSyncAction');
      const message = this.translate.instant(model.departments.length > 1 ? 'OneRoster.ExecuteConfirmMessageMany' : 'OneRoster.ExecuteConfirmMessageOne', {
        departments: model.departments.length,
        academicYear: model.academicYear.name,
        academicPeriod: model.academicPeriod.name
      });
      if (model.name == null || model.name === '') {
        const translateDescription = model.departments.length === 1 ? 'OneRoster.SynActionDefaultNameOne' : 'OneRoster.SynActionDefaultNameMany';
        model.name = this.translate.instant(translateDescription, {
          departments: model.departments.length,
          academicYear: model.academicYear ? model.academicYear.name : '-',
          academicPeriod: model.academicPeriod ? model.academicPeriod.name : '-'
        });
      }
      return this.modal.showDialog(title, message, DIALOG_BUTTONS.YesNo).then((result) => {
        if (result === 'yes') {
          this.loading.showLoading()
          model.actionStatus = {
            alternateName: 'PotentialActionStatus'
          }
          return this.context.model('OneRosterSyncActions').save({
            id: current.id,
            startTime: null,
            endTime: null,
            actionStatus: {
              alternateName: 'PotentialActionStatus'
            },
          }).then(({id}) => {
            return this.context.model('OneRosterSyncActions').save({
              id,
              startTime: new Date(),
              endTime: null,
              actionStatus: {
                alternateName: 'ActiveActionStatus'
              }
            }).then(() => {
              this.loading.hideLoading();
              const message = this.translate.instant('OneRoster.ExecuteSubmitMessage');
              this.toast.show(title, message);
              this.router.navigate([ '..' ], { relativeTo: this.activatedRoute });
            });
          });
        }
      }).catch((err) => {
        this.loading.hideLoading();
        this.errorService.showError(err);
      });
    });
    
  }

}
