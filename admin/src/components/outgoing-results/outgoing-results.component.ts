import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationService } from '@universis/common';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'one-roster-outgoing-results',
  templateUrl: './outgoing-results.component.html'
})
export class OutgoingResultsComponent implements OnInit, AfterViewInit, OnDestroy {
  public subscription?: any;
  public student?: any;
  public results?: any[];
  public loading: boolean = true;
  ngOnDestroy(): void {
    //
  }
  ngAfterViewInit(): void {
    //
    this.subscription = this.activatedRoute.params.subscribe((params) => {
      this.student = params.id;
      this.context.model(`Students/${this.student}/OutgoingResults`).getItems().then((results) => {
        this.results = results;
        this.loading = false;
      }).catch((err) => {
        console.error(err);
        this.loading = false;
      });
    });
  }

  constructor(private context: AngularDataContext,
    private configuration: ConfigurationService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

  }

}
