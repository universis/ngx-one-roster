import { Component, OnInit, Input, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedTableComponent } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { AdvancedFormComponent, ServiceUrlPreProcessor } from '@universis/forms';
import { ClientDataQueryable } from '@themost/client';

@Component({
  selector: 'universis-one-roster-action-new',
  templateUrl: './action-new.component.html',
  styles: [
    `
    app-table-simple-header {
      .btn.btn-action.disabled, .btn.btn-action:disabled {
        border-color: none !important;
      }
    }
    `
  ],
  encapsulation: ViewEncapsulation.None
})
export class ActionNewComponent implements OnInit, AfterViewInit {

  formConfig?: any;
  error?: any;

  constructor(private context: AngularDataContext,
    private translate: TranslateService,
    private modal: ModalService,
    private toast: ToastService,
    private errorService: ErrorService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private loading: LoadingService) { }
  ngAfterViewInit(): void {
    // const form = new ServiceUrlPreProcessor(this.context).parse(executeForm);
    // this.formConfig = form;
  }

  @Input() model: any = {

  };

  @ViewChild('table') table?: AdvancedTableComponent;
  @ViewChild('form') form?: AdvancedFormComponent;

  ngOnInit() {
  }

  private async getSelectedItems() {
    let items: { id: number }[] = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter((item: any) => {
              if (this.table == null) {
                return true;
              }
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item: any) => {
            return {
              id: item.id
            };
            
          });
        }
      }
    }
    return items;
  }

  submit() {
    if (this.table == null) {
      return;
    }
    this.model.academicYear = this.model.academicYear || {};
    this.model.academicPeriod = this.model.academicPeriod || {};

    if (this.model.academicYear.id == null || this.model.academicPeriod.id == null) {
      this.error = new Error(this.translate.instant('OneRoster.SelectAcademicYearPeriod'));
      return;
    }
    delete this.error;
    // set departments
    void this.getSelectedItems().then((departments: any[]) => {
      this.model.departments = departments;
      const title = this.translate.instant('OneRoster.ExecuteSyncAction');
      const message = this.translate.instant(this.model.departments.length > 1 ? 'OneRoster.ExecuteConfirmMessageMany' : 'OneRoster.ExecuteConfirmMessageOne', {
        departments: this.model.departments.length,
        academicYear: this.model.academicYear.name,
        academicPeriod: this.model.academicPeriod.name
      });
      if (this.model.name == null || this.model.name === '') {
        const translateDescription = this.model.departments.length === 1 ? 'OneRoster.SynActionDefaultNameOne' : 'OneRoster.SynActionDefaultNameMany';
        this.model.name = this.translate.instant(translateDescription, {
          departments: this.model.departments.length,
          academicYear: this.model.academicYear.name,
          academicPeriod: this.model.academicPeriod.name
        });
      }
      return this.modal.showDialog(title, message, DIALOG_BUTTONS.YesNo).then((result) => {
        if (result === 'yes') {
          this.loading.showLoading();
          const newItem = Object.assign({
            actionStatus: {
              alternateName: 'PotentialActionStatus',
            }
          }, this.model);
          return this.context.model('OneRosterSyncActions').save(newItem).then(({id}) => {
            return this.context.model('OneRosterSyncActions').save({
              id,
              actionStatus: {
                alternateName: 'ActiveActionStatus'
              }
            }).then(() => {
              this.loading.hideLoading();
              const message = this.translate.instant('OneRoster.ExecuteSubmitMessage');
              this.toast.show(title, message);
              this.router.navigate([ '..' ], { relativeTo: this.activatedRoute });
            });
          });
        }
      });
    }).catch((err) => {
      this.loading.hideLoading();
      this.errorService.showError(err);
    });
    
  }

  change(event: any) {
    Object.assign(this.model, event);
  }

}
