import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService, GradeScale } from '@universis/common';

@Component({
  selector: 'one-roster-service-incoming-results',
  templateUrl: './service-incoming-results.component.html',
  styleUrls: [
  ]
})
export class ServiceIncomingResultsComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() serviceProvider?: { name: string, alternateName: string, url: string};
  public loading = true;
  public student?: any;
  public results?: any[];
  subscription: any;

  constructor(private context: AngularDataContext,
    private configuration: ConfigurationService,
    private activatedRoute: ActivatedRoute) { }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  ngAfterViewInit(): void {
    this.subscription = this.activatedRoute.params.subscribe((params) => {
      this.student = params.id;
      if (this.serviceProvider) {
        this.context.model(`Students/${this.student}/IncomingResults`).save({
          provider: this.serviceProvider.alternateName
        }).then((results) => {
          // get study program
          return this.context.model(`Students/${this.student}/studyProgram`).asQueryable().expand(
            'gradeScale($expand=values)'
          ).getItem().then((studyProgram) => {
            if (studyProgram && studyProgram.gradeScale) {
              const gradeScale = new GradeScale(this.configuration.currentLocale, studyProgram.gradeScale);
              for (const result of results) {
                // prepare course metadata
                result.lineItem.class.metadata = result.lineItem.class.metadata || {};
                result.lineItem.class.course.metadata = result.lineItem.class.course.metadata || {};
                if (result.score != null && result.score >=0 && result.score <= 100) {
                  result.metadata = result.metadata || {};
                  result.metadata.altScoreText = gradeScale.format(result.score/100);
                }
                // try to get local course (and class)
                this.context.model('Course').where('oneRoster/sourcedId')
                  .equal(result.lineItem.class.course.sourcedId)
                  .getItem().then((course) => {
                  result.lineItem.class.course.metadata.course = course ? course : null
                });
                this.context.model('CourseClass').where('oneRoster/sourcedId')
                  .equal(result.lineItem.class.sourcedId)
                  .getItem().then((courseClass) => {
                  result.lineItem.class.metadata.courseClass = courseClass ? courseClass : null
                });
              }
            }
            this.results = results;
            this.loading = false;
          });
        }).catch((err) => {
          console.error(err);
          this.loading = false;
        });
      }
    });
    
  } 

  ngOnInit() {
  }

  assign() {
    
  }

}
