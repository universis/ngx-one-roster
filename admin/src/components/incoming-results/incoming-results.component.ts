import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'one-roster-incoming-results',
  templateUrl: './incoming-results.component.html',
  styleUrls: [
  ]
})
export class IncomingResultsComponent implements OnInit, AfterViewInit {

  public unsupported?: boolean;
  public unknownProvider?: boolean;
  public serviceProviders?: any[];
  public loading = true;

  constructor(private context: AngularDataContext) { }
  ngAfterViewInit(): void {
    //
    this.context.getMetadata().then((schema) => {
      const action = schema.Action.find((action) => action.Name === 'IncomingResults' && action.Parameter.findIndex((param) => {
        return param.Name === 'bindingParameter' && param.Type === 'Student';
      }) > -1);
      this.unsupported = (action == null);
      // get service providers
      this.context.model('OneRosterServiceProviders').getItems().then((items) => {
        this.unknownProvider = (items.length === 0);
        this.serviceProviders = items;
      });
    });
  } 

  ngOnInit() {
  }

}
