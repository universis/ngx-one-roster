import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ServerEventService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedTableComponent } from '@universis/ngx-tables';
import { AdvancedFormComponent } from '@universis/forms';

@Component({
  selector: 'universis-one-roster-action-monitor',
  templateUrl: './action-monitor.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ActionMonitorComponent implements OnInit, AfterViewInit, OnDestroy {

   public events: Array<any> = [
   ]; 
  subscription: any;
  
   constructor(private context: AngularDataContext,
    private translate: TranslateService, private serverEvent: ServerEventService) { }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  ngAfterViewInit(): void {
    this.subscription = this.serverEvent.message.subscribe((event) => {
        if (event && event.additionalType === 'OneRosterSyncAction') {
          // get message text
          const message = event.message.slice(-1);
          this.events.unshift({
            date: new Date(),
            message: `${message}` 
          });
        }
      });
  }

  @ViewChild('table') table?: AdvancedTableComponent;
  @ViewChild('form') form?: AdvancedFormComponent;

  ngOnInit() {
  }

}
