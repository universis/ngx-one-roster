import { Component, OnInit, ViewChild, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { AdvancedTableComponent, AdvancedRowActionComponent } from '@universis/ngx-tables';
import { ModalService, ErrorService, ServerEventService } from '@universis/common';
import { Observable } from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'universis-one-roster-action-list',
  templateUrl: './action-list.component.html',
  styles: []
})
export class ActionListComponent implements OnInit, AfterViewInit {

  @ViewChild('table') table?: AdvancedTableComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() formData = {};

  constructor(private modalService: ModalService,
    private context: AngularDataContext,
    private errorService: ErrorService,
    private translateService: TranslateService,
    private activateRoute: ActivatedRoute,
  private router:Router) { }
  ngAfterViewInit(): void {
    //
  }

  ngOnInit() {
  }

  executeAddSyncAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const component = <AdvancedRowActionComponent>this.modalService.modalRef.content;
      if (component) {
        this.formData = component.data;
      }
      // execute promises in series within an async method
      (async () => {
        this.refreshAction.emit({
          progress: 100
        });
        await this.context.model('OneRosterSyncActions').save(this.formData);
        if (this.table) {
          this.table.fetch();
        }
      })().then(() => {
        observer.next();
      }).catch((err) => {
        observer.error(err);
      });
    });

  }

  addSyncAction() {
    return this.router.navigate(['new'], { relativeTo: this.activateRoute });
  }

  refresh() {
    if (this.table) {
      this.table.fetch();
    }
  }


}
