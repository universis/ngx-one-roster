# @universis/ngx-one-roster

Client-side components of [@universis/one-roster](https://gitlab.com/universis/one-roster)

## Usage
```shell
npm i @universis/ngx-one-roster
```

Edit `angular.json` to include `@universis/ngx-one-roster` assets at `architect.build.options.assets`:

```json
{
    "assets": [
        {
            "glob": "**/*",
            "input": "node_modules/@universis/ngx-one-roster/assets/",
            "output": "assets"
        }
    ]
}


### Development

Import `@universis/ngx-one-roster` as submodule in any angular cli project by replacing `newProjectRoot` as already configured in your `angular.json`

```shell
git submodule add https://gitlab.com/ngx-one-roster/common.git <newProjectRoot>/ngx-one-roster
```

e.g.

```shell
git submodule add https://gitlab.com/universis/ngx-one-roster.git packages/ngx-one-roster
```

Add the following entries to `tsconfig.app.json#compilerOptions.paths`:
```json
{
    "compilerOptions": {
        "paths": {
            "@universis/ngx-one-roster/admin": [
                "<newProjectRoot>/ngx-one-roster/admin/public_api"
            ],
            "@universis/ngx-one-roster": [
                "<newProjectRoot>/ngx-one-roster/public_api"
            ]
        }
    }
}  
```

Edit `angular.json` to include `@universis/ngx-one-roster` assets at `architect.build.options.assets`:

```json
{
    "assets": [
        {
            "glob": "**/*",
            "input": "<newProjectRoot>/ngx-one-roster/admin/src/assets/",
            "output": "assets"
        }
    ]
}